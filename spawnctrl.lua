-- spawnctrl
-- Spawns a mob at a provided xyz coordinates, based on a queue

-- CONFIG
local time_limit = 120
local delay = 1 -- delay between HTTP polls
-- default position to spawn is right next to the command computer, can be overriden from startup
local px = '~2'
local py = '~2'
local pz = '~-2'

local url = 'http://drupaul.dk/json/' -- queue URL, minus queueid


-- HELPERS
function getQueue(queueid)
  local handle = http.get(url..queueid)
  if (handle ~= nil) then
    code = handle.getResponseCode()
    handle.close()
    if (code == 200) then
      return true
    end
  end
  return false
end


-- MAIN
-- Get arguments
local args = { ... }
if (#args < 2) then
  print( 'USAGE: spawnctrl <queueid> <EntityID> [x] [y] [z]' )
  return
end
local queueid = args[1]
local entityid = args[2]

if args[3] and args[4] and args[5] then
  px = args[3]
  py = args[4]
  pz = args[5]
end

print('Running spawnctrl application with queueid '..queueid..'. Hold CTRL + T to end.')
while (true) do
  local status = getQueue(queueid)
  if (status == true) then
    local r = math.random(2)
    if r == 1 then
      -- summon baby 50% of the time
      commands.summon(entityid, px, py, pz, '{Age: -1000}')
      commands.summon(entityid, px, py, pz, '{Age: -1000}')
    else
      commands.summon(entityid, px, py, pz)
      commands.summon(entityid, px, py, pz)
    end
  end
  sleep(delay)
end
