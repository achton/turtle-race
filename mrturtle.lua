-- mrturtle
-- Mr. Turtle is a script which allows control of a turtle using 4 separate queues.
-- It takes 4 parameters, which are the numeric queue ids for the directions:
-- forward, right, back, left

-- CONFIG
delay = 1 -- delay between polls

url = 'http://drupaul.dk/json/' -- queue URL, minus queueid

-- HELPERS
-- Helper function to poll a single queue URL and return status (bool).
function getQueue(url)
  local handle = http.get(url)
  if (handle ~= nil) then
    code = handle.getResponseCode()
    handle.close()
    if (code == 200) then
      return true
    end
  end
  return false
end

-- Helper function to move the turtle in some direction.
function walkTurtle(direction)
  if (direction == 'forward') then
    turtle.forward()
  elseif (direction == 'right') then
    turtle.turnRight()
    turtle.forward()
  elseif (direction == 'left') then
    turtle.turnLeft()
    turtle.forward()
  elseif (direction == 'back') then
    turtle.back()
  elseif (direction == 'up') then
    turtle.up()
  elseif (direction == 'down') then
    turtle.down()
  end
end

-- Poll each queue and return status for all directions.
-- TODO: Adjust API so it's possible to retrieve all directions with 1 request.
function getQueues(qf, qr, ql, qb, qu, qd)
  f_status = getQueue(url..qf)
  r_status = getQueue(url..qr)
  l_status = getQueue(url..ql)
  b_status = getQueue(url..qb)
  -- Up/down queues are optional
  u_status = false
  d_status = false
  if (qu ~= nil and qd ~= nil) then
    u_status = getQueue(url..qu)
    d_status = getQueue(url..qd)
  end
  directions = { forward = f_status, right = r_status, left = l_status, back = b_status, up = u_status, down = d_status }
  return directions
end

-- MAIN
-- Get queueid arguments
local args = { ... }
if (#args < 4 or #args ~= 6) then
  print( "USAGE: mrturtle <queueid_fwd> <queueid_right> <queueid_left> <queueid_back> [queueid_up] [queueid_down]" )
  return
end

local qf = args[1]
local qr = args[2]
local ql = args[3]
local qb = args[4]
local qu = nil -- default is off
local qd = nil -- default is off
if (#args == 6) then
  qu = args[5]
  qd = args[6]
  print('Mr. Turtle is ready for a walk. Using queues '..qf..', '..qr..', '..ql..', '..qb..', '..qu..', '..qd..'. Hold CTRL + T to end.')
else
  print('Mr. Turtle is ready for a walk. Using queues '..qf..', '..qr..', '..ql..', '..qb..'. Hold CTRL + T to end.')
end

while (true) do
  local directions = getQueues(qf, qr, ql, qb, qu, qd)
  for direction,do_walk in pairs(directions) do
    if (do_walk == true) then
      walkTurtle(direction)
    end
  end
  sleep(delay)
end
