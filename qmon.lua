-- qmon
-- Monitor the queue status via its JSON API and output to an attached terminal

-- CONFIG
local textscale = 1.5 -- default monitor text size, from 0.5 to 5.0, can be overridden

local status_url = 'http://drupaul.dk/orders/json'
local delay = 3 -- seconds to wait between polls


-- HELPERS
os.loadAPI('json')

-- Fetch JSON data from URL
function fetchData()
  local str = http.get(status_url).readAll()
  local data = json.decode(str)
  return data
end

-- Write a header for the table
function writeHeader()
  monitor.clear()
  monitor.setCursorPos(1,1)
  monitor.write('QUEUE STATUS')
  monitor.setCursorPos(1,2)
  monitor.write('--------------------')
end

-- MAIN
-- Get arguments
local args = { ... }
if (#args == 1) then
  textscale = tonumber(args[1])
end

print('Running qmon. Hold CTRL + T to end.')

monitor = peripheral.find('monitor')
monitor.clear()
monitor.setTextScale(textscale)

while true do
  writeHeader()
  local orders = fetchData()
  local next = next
  local offset = 2 -- vertical offset (lines) to begin printing orders
  if (next(orders) == nil) then
    monitor.setCursorPos(1,3)
    monitor.write('No orders found.\n')
  else
    for k,v in pairs(orders) do
        monitor.setCursorPos(1,k+offset)
        monitor.write(v['name']..': '..v['field_queue']..'\n')
    end
  end
  sleep(delay)
end

