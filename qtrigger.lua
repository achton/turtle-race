-- qtrigger
-- Trigger an RS output based on a HTTP request and an RS input.
-- It's a complicated AND-gate, basically.

-- CONFIG
side_out = 'back' -- default side to output signal to (override from cmdline)
side_in = 'left' -- default side to require input from (override from cmdline)
delay = 1 -- delay between callbacks

url = 'http://drupaul.dk/json/' -- queue URL, minus queueid

-- HELPERS
function getQueue(queueid)
  local handle = http.get(url..queueid)
  if (handle ~= nil) then
    code = handle.getResponseCode()
    handle.close()
    if (code == 200) then
      return true
    end
  end
  return false
end

-- Toggle the output
function toggleOutput()
  local signal = rs.getOutput(side_out)
  rs.setOutput(side_out, not signal)
end

-- MAIN
-- Get arguments
local args = { ... }
if (#args < 1) then
  print( "USAGE: qtrigger <queueid> [side_out] [side_in]" )
  return
end
local queueid = args[1]
if args[2] then
  side_out = args[2]
  if args[3] then
    side_in = args[3]
  end
end

print('Running qtrigger application with queueid '..queueid..'. Output to '..side_out..', input expected from '..side_in..'. Hold CTRL + T to end.')
while (true) do
  local status = getQueue(queueid)
  if (status == true and rs.getInput(side_in)) then
    toggleOutput()
  end
  sleep(delay)
end
