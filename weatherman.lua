-- weatherman
-- Sets the weather to a random type

-- CONFIG
local time_limit = 120 -- default no. of seconds to set the weather for
local delay = 1 -- delay between HTTP polls

local weather_types = {'clear', 'rain', 'thunder'}

local url = 'http://drupaul.dk/json/' -- queue URL, minus queueid


-- HELPERS
function getQueue(queueid)
  local handle = http.get(url..queueid)
  if (handle ~= nil) then
    code = handle.getResponseCode()
    handle.close()
    if (code == 200) then
      return true
    end
  end
  return false
end


-- MAIN
-- Get arguments
local args = { ... }
if (#args ~= 1) then
  print( "USAGE: weatherman <queueid>" )
  return
end
local queueid = args[1]

print('Running weatherman application with queueid '..queueid..'. Hold CTRL + T to end.')
while (true) do
  local status = getQueue(queueid)
  if (status == true) then
    local r =  math.random(3)
    commands.weather(weather_types[r], time_limit)
  end
  sleep(delay)
end
