-- fw_parts
-- Put this in the Turtle that supplies parts to the fw_maker turtle.
-- Start fw_maker first, then start this program!

side = 'left'
delay = 0.2

firstColor = 1
lastColor = 8
paper = 9
firstSpecial = 10
lastSpecial = 12
firstGunpowder = 13
lastGunpowder = 16

function randomInt(min, max)
  return min + math.floor(math.random() * (max - min + 1))
end

function randomColor()
  return randomInt(firstColor, lastColor)
end

function randomGunpowder()
  return randomInt(firstGunpowder, lastGunpowder)
end

function sync()
  rs.setOutput(side, true)
  sleep(delay)
  rs.setOutput(side, false)
  sleep(delay)
  while not rs.getInput(side) do
    sleep(delay)
  end
  sleep(delay * 3)
end

function give(n, optional)
  -- print('giving #', n);
  turtle.select(n)
  if not optional and turtle.getItemCount(n) == 0 then
    print('Refill slot #', n)
    while turtle.getItemCount(n) == 0 do
      sleep(delay)
    end
  end
  return turtle.drop(1)
end

function giveThings(first, last, start)
  start = start or 0
  d = last - first
  n = 2^d
  r = randomInt(start, n - 1)
  for i = 0, d - 1 do
-- print('doing BAND with # and #', r, 2^i)
    if bit.band(r, 2^i) ~= 0 then
      give(first + i, true)
    end
  end
end

-- START PROCEDURE
while true do
  give(randomColor())
  give(randomGunpowder())
  giveThings(firstSpecial, lastSpecial)
  sync()
  give(randomGunpowder())
  give(paper)
  sync()
end
