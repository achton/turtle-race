-- fw_maker
-- Put this in the Crafty Turtle that makes the fireworks.

-- Start this one first, then start fw_parts!

-- CONFIG
side_in = 'front' -- default side to require input from (override from cmdline)

side_comms = 'back' -- no need to change
delay = 0.2

-- HELPERS
function repack()
  -- if something is in cell 4, move it to cell 11.
  turtle.select(4)
  turtle.transferTo(11)
  -- if something is in cell 8, move it to cell 10.
  turtle.select(8)
  turtle.transferTo(10)
  -- select cell 1
  turtle.select(1)
end

function sync()
  rs.setOutput(side_comms, true)
  sleep(delay)
  rs.setOutput(side_comms, false)
  sleep(delay)
  while not rs.getInput(side_comms) do
    sleep(delay)
  end
  sleep(delay * 3)
  repack()
end

function getQueue(queueid)
  local handle = http.get("http://drupaul.dk/json/"..queueid)
  code = handle.getResponseCode()
  if (code == 200) then
    return true
  else
    return false
  end
end

function getMasterRS()
  return rs.getInput(side_in)
end

function goForLaunch(queueid)
  rsm = getMasterRS()
  qstatus = false
  if (rsm == true) then
    qstatus = getQueue(queueid)
  end
  return qstatus
end

-- MAIN
-- Get arguments
local args = { ... }
if (#args < 1) then
  print( "USAGE: fw_maker <queueid> [side_in]" )
  return
end
local queueid = args[1]
if args[2] then
  side_in = args[2]
end

print('Running fw_maker with queueid '..queueid..'. RS input expected from '..side_in..'. Hold CTRL + T to end.')

-- drop all items first
for i=1,16 do
  turtle.select(i)
  turtle.drop()
end
-- select cell 1
turtle.select(1)

while (true) do
  sync() -- firework star
  turtle.craft(1)
  sleep(delay)
  sync() -- firework!
  turtle.craft(1)
  while not (goForLaunch(queueid)) do
    sleep(delay*5)
  end
  turtle.place()
end
