-- startup
-- A bootstrapper for The Turtle Game

-- Initialize/update this script with:
--   pastebin get 4nRg9CHU json
--   pastebin get e9KNyX8A startup
--   edit startup
--   <insert the scriptname you want to run on this machine at top and save>
--   reboot
-- Update the script:
--   rm startup
--   pastebin get e9KNyX8A startup


-- This will fetch an updated version of the script and reboot the machine so
-- that it starts the script. To update the script you chose from Gitlab, simply
-- reboot the machine. It will pull down a new version automagically.

-- CONFIG
-- Enter name of the script for this machine, eg. 'qmon'. It must be available
-- in the Gitlab repo as <scriptname>.lua
scriptname = ''
-- Enter any parameters for that script as a table, eg. {'2', 'front', 'left'}
parameters = {}

-- HELPERS
os.loadAPI('json')

-- base64 decoding
function b64dec(data)
    -- character table string
  local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
  data = string.gsub(data, '[^'..b..'=]', '')
  return (data:gsub('.', function(x)
    if (x == '=') then return '' end
    local r,f='',(b:find(x)-1)
    for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
    return r;
  end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
    if (#x ~= 8) then return '' end
    local c=0
    for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
    return string.char(c)
  end))
end

-- Fetch provided script from Gitlab.com and return file content
function getScript(scriptname)
  local url = 'https://gitlab.com/api/v3/projects/636967/repository/files?private_token=JyHuJyzsVikgGsfDx8Sx&ref=master&file_path='..scriptname..'.lua'
  -- Make HTTP GET request to the url
  local response = http.get(url)
  if (response == nil or response == '') then
  	print('ERROR: HTTP GET failed, check url: "'..url..'"')
  	return false
  end
  -- Read the contents of the HTTP body in response
  local content = response.readAll()
  if (content == nil or content == '') then
    print('ERROR: Gitlab response empty, check url: "'..url..'"')
    return false
  else
    local jsondata = json.decode(content)
    local decoded = b64dec(jsondata['content'])
    -- return friggin HTTP response which is json-decoded base64-decoded file content from Gitlab in Lua inside a VM in Minecraft in Java, mofo!
    return decoded
  end
end

-- Write a string to the filesystem
function writeScript(content, scriptname)
  local h = fs.open(scriptname, "w")
  if (h ~= nil) then
    h.writeLine(content)
    h.close()
  else
    print ('ERROR: Could not write file "'..scriptname..'"')
  end
end


-- MAIN
if (scriptname ~= '') then
  print('Running startup bootstrapper...')
  print('Fetching script "'..scriptname..'" from Gitlab...')
  local content = getScript(scriptname)
  if (content ~= false) then
    print('Writing script to filesystem in file "'..scriptname..'".')
    writeScript(content, scriptname)
  end
  -- execute script
  table.insert(parameters, 1, scriptname) -- insert script name in front of params
  print('Executing script: "'..table.concat(parameters, ' ')..'"')
  shell.run(unpack(parameters))
else
  print('To use the startup auto-update bootstrapper, edit "startup" and insert name of script to run on reboot.')
end
